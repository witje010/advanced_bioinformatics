#!usr/bin/evn python

from sys import argv

def countnucleotides(string):
    a, t, c, g = 0, 0, 0, 0
    string = string.lower()
    for ch in string:
        if ch == "a":
            a += 1
        elif ch == "t":
            t += 1
        elif ch == "c":
            c += 1
        elif ch == "g":
            g += 1
    line = "{} {} {} {}".format(str(a), str(c), str(g), str(t))
    print line

if __name__ == "__main__":
    filename = argv[1]
    thefile = open(filename, "r")
    for line in thefile:
        countnucleotides(line)