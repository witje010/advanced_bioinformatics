#!/usr/bin/evn python
"""
Author: Lotte Witjes
Student number: 950405 966 120

Returns the number of possible mRNAs that could have been translated to the input
sequence %1000000.

"""

from sys import argv

dictionary = {"F":["UUU", "UUC"], "L":["UUA", "UUG", "CUU", "CUC", "CUA", "CUG"],\
              "I":["AUU", "AUC", "AUA"], "M":["AUG"], "V":["GUU", "GUC", "GUA",\
             "GUG"], "S":["UCU", "UCC", "UCA", "UCG", "AGU", "AGC"], "P":["CCU",\
             "CCC", "CCA", "CCG"], "T":["ACU", "ACC", "ACA", "ACG"], "A":["GCU",\
              "GCC", "GCA", "GCG"], "Y":["UAU", "UAC"], "STOP":["UAA", "UAG",\
            "UGA"], "H":["CAU", "CAC"], "Q":["CAA", "CAG"], "N":["AAU", "AAC"],\
              "K":["AAA", "AAG"], "D":["GAU", "GAC"], "E":["GAA", "GAG"],\
              "C":["UGU", "UGC"], "R":["CGU", "CGC", "CGA", "CGG", "AGA", "AGG"],\
              "W":["UGG"], "G":["GGU", "GGC", "GGA", "GGG"]}

def possibleRNAs(inputSet):
    inputProtein = open(inputSet, "r").read().strip()
    possibilities = len(dictionary["STOP"])
    for i in inputProtein:
        possibilities *= len(dictionary[i])
    return possibilities

if __name__ == "__main__":
    inputSet = argv[1]
    print possibleRNAs(inputSet) % 1000000

