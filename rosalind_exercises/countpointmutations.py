#!/usr/bin/evn python

from sys import argv

def countPointMutations(sequence1, sequence2):
    point_mutations = 0
    for i in range(len(sequence1)):
            if not sequence1[i] == sequence2[i]:
                point_mutations += 1
    return point_mutations

def makeListOfLines(filename):
    alist = []
    for line in filename:
        line = line.strip("\n")
        alist.append(line)
    return alist

if __name__ == "__main__":
    filename = argv[1]
    thefile = open(filename, "r")
    lines = makeListOfLines(thefile)
    seq1, seq2 = lines
    result = countPointMutations(seq1, seq2)
    print result
    
