#!/usr/bin/evn python
"""
Author = Lotte Witjes
Student number = 950405 966 120

A program that returns the position and length of every reverse palindrome in
the DNA sequence having a length between 4 and 12 (4, 6, 8, 10, 12).
"""

from sys import argv
import re

def fastaparser(filename):
    inFile = open(filename, "r")
    seqs = {}
    for line in inFile:
        if line.startswith('>'):
            label = line.strip().replace(">","")
            seqs[label] = ""
        else:
            seqs[label] += line.strip()
    return seqs

def extract_kmers(size):
    seqs = fastaparser(filename)
    kmer_size = size
    ch = {} #dict to store characters
    res = [] #dict to store k-mers and counts
    for label, seq in seqs.items():
        for c in seq:
            if c not in ch:
                ch[c] = 0
            ch[c] += 1
        for i in range(len(seq)-kmer_size+1):
            kmer = seq[i:i+kmer_size]
            count = True
            for c in kmer:
                if c not in "TGAC":
                    count = False
            if count is False:
                continue
            if kmer not in res:
                res.append(kmer)
    return res
    
def isPalindrome(kmers):
    palindromes = []
    for i in range(len(kmers)):
        revcompkmer = complement(kmers[i])
        if kmers[i] == revcompkmer:
            palindromes.append(kmers[i])
    return palindromes

def complement(string):
    string = string.upper()
    reverse_complement = ""
    for ch in string:
        if ch == "A":
            reverse_complement = "T" + reverse_complement
        elif ch == "T":
            reverse_complement = "A" + reverse_complement
        elif ch == "C":
            reverse_complement = "G" + reverse_complement
        elif ch == "G":
            reverse_complement = "C" + reverse_complement
    return reverse_complement

def findIndices(palindromes, DNAsequence):
    
    dictionary = {}
    for i in range(len(palindromes)):
        pattern = re.compile(r"(?="+palindromes[i]+")")
        matches = re.finditer(pattern, DNAsequence)
        for j in matches:
            index = j.start() + 1
            print index, len(palindromes[i])
        
            
if __name__ == "__main__":
    filename = argv[1]
    seqs = fastaparser(filename)

    sequences = []
    for key in seqs:
        sequences.append(seqs[key])
    
    kmers_4 = extract_kmers(4)
    kmers_6 = extract_kmers(6)
    kmers_8 = extract_kmers(8)
    kmers_10 = extract_kmers(10)
    kmers_12 = extract_kmers(12)

    palindromes_4 = isPalindrome(kmers_4)
    palindromes_6 = isPalindrome(kmers_6)
    palindromes_8 = isPalindrome(kmers_8)
    palindromes_10 = isPalindrome(kmers_10)
    palindromes_12 = isPalindrome(kmers_12)

    for i in range(len(sequences)):
        findIndices(palindromes_4, sequences[i])
        findIndices(palindromes_6, sequences[i])
        findIndices(palindromes_8, sequences[i])
        findIndices(palindromes_10, sequences[i])
        findIndices(palindromes_12, sequences[i])
