#!/usr/bin/evn python
"""Returns c**2 with a given a and b, using equation of Pythagoras."""

from sys import argv

def pythagoras(a, b):
    c2 = (a**2)+(b**2)
    return c2

if __name__ == "__main__":
    filename = argv[1]
    thefile = open(filename, "r")
    for line in thefile:
        a, b = line.split()
        a = int(a)
        b = int(b)
    print pythagoras(a, b)