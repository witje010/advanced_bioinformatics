#!/usr/bin/evn python
"""
Author: Lotte Witjes
Translates a RNA sequence to a protein sequence only in the first forward frame.
"""

from sys import argv
import re

table = {"UUU":"F", "UUC":"F", "UUA":"L", "UUG":"L",
    "UCU":"S", "UCC":"S", "UCA":"S", "UCG":"S",
    "UAU":"Y", "UAC":"Y", "UAA":"STOP", "UAG":"STOP",
    "UGU":"C", "UGC":"C", "UGA":"STOP", "UGG":"W",
    "CUU":"L", "CUC":"L", "CUA":"L", "CUG":"L",
    "CCU":"P", "CCC":"P", "CCA":"P", "CCG":"P",
    "CAU":"H", "CAC":"H", "CAA":"Q", "CAG":"Q",
    "CGU":"R", "CGC":"R", "CGA":"R", "CGG":"R",
    "AUU":"I", "AUC":"I", "AUA":"I", "AUG":"M",
    "ACU":"T", "ACC":"T", "ACA":"T", "ACG":"T",
    "AAU":"N", "AAC":"N", "AAA":"K", "AAG":"K",
    "AGU":"S", "AGC":"S", "AGA":"R", "AGG":"R",
    "GUU":"V", "GUC":"V", "GUA":"V", "GUG":"V",
    "GCU":"A", "GCC":"A", "GCA":"A", "GCG":"A",
    "GAU":"D", "GAC":"D", "GAA":"E", "GAG":"E",
    "GGU":"G", "GGC":"G", "GGA":"G", "GGG":"G",}

def translate(sequence):
    sequence = sequence.upper()
    proteinseq = ""
    pattern = re.compile(r"[AUCG][AUCG][AUCG]")
    codons = pattern.findall(sequence)
    for codon in codons:
        if codon in table.keys():
            aminoacid = table[codon]
            proteinseq += aminoacid
    if "STOP" in proteinseq:
        proteinseq = proteinseq.strip("STOP")
    return proteinseq

if __name__ == "__main__":
    filename = argv[1]
    thefile = open(filename, "r")
    for line in thefile:
        print translate(line)
