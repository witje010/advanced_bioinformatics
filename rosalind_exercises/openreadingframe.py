#!/usr/bin/evn python
"""
Author: Lotte Witjes
Returns all possible open reading frames of a DNA sequence
"""

from sys import argv
import re

table = {
    'ATA':'I', 'ATC':'I', 'ATT':'I', 'ATG':'M',
    'ACA':'T', 'ACC':'T', 'ACG':'T', 'ACT':'T',
    'AAC':'N', 'AAT':'N', 'AAA':'K', 'AAG':'K',
    'AGC':'S', 'AGT':'S', 'AGA':'R', 'AGG':'R',
    'CTA':'L', 'CTC':'L', 'CTG':'L', 'CTT':'L',
    'CCA':'P', 'CCC':'P', 'CCG':'P', 'CCT':'P',
    'CAC':'H', 'CAT':'H', 'CAA':'Q', 'CAG':'Q',
    'CGA':'R', 'CGC':'R', 'CGG':'R', 'CGT':'R',
    'GTA':'V', 'GTC':'V', 'GTG':'V', 'GTT':'V',
    'GCA':'A', 'GCC':'A', 'GCG':'A', 'GCT':'A',
    'GAC':'D', 'GAT':'D', 'GAA':'E', 'GAG':'E',
    'GGA':'G', 'GGC':'G', 'GGG':'G', 'GGT':'G',
    'TCA':'S', 'TCC':'S', 'TCG':'S', 'TCT':'S',
    'TTC':'F', 'TTT':'F', 'TTA':'L', 'TTG':'L',
    'TAC':'Y', 'TAT':'Y', 'TAA':'_', 'TAG':'_',
    'TGC':'C', 'TGT':'C', 'TGA':'_', 'TGG':'W',
    }

def fastaparser(thefile):
    inFile = open(thefile)
    seqs = {}
    for line in inFile:
	if line.startswith('>'):
	    label = line.strip().replace(">","")
	    seqs[label] = ""
	else:
	    seqs[label] += line.strip()
    return seqs
    

def openreadingframes(sequence):
    sequence = sequence.upper()
    startcodons = re.compile(r"ATG")
    stopcodons = re.compile(r"TAA|TAG|TGA")
    index_startcodons = re.finditer(startcodons, sequence)
    index_stopcodons = re.finditer(stopcodons, sequence)

    index_start = []
    index_stop = []
    for i in index_startcodons:
        index = i.start()
        index_start.append(index)
    for i in index_stopcodons:
        index = i.start()
        index_stop.append(index)

    ORFS = []
    for i in index_start:
        for j in index_stop:
            orf = sequence[i:j]
            if orf in ORFS:
                continue
            else:
                codon = re.compile(r"[ATCG][ATCG][ATCG]")
                codons = codon.findall(orf)
                if "TAA" in codons or "TAG" in codons or "TGA" in codons:
                    continue
                else:
                    if len(orf) >= 3 and len(orf)%3 == 0:
                          ORFS.append(orf)
    return ORFS

def translateorfs(sequence):
    ORFS = openreadingframes(sequence)
    translatedORFS = []
    for i in ORFS:
        proteinseq = ""
        codon = re.compile(r"[ATCG][ATCG][ATCG]")
        codons = codon.findall(i)
        for i in codons:
            aminoacid = table[i]
            proteinseq += aminoacid
        translatedORFS.append(proteinseq)
    return translatedORFS

def complement(string):
    string = string.upper()
    reverse_complement = ""
    for ch in string:
        if ch == "A":
            reverse_complement = "T" + reverse_complement
        elif ch == "T":
            reverse_complement = "A" + reverse_complement
        elif ch == "C":
            reverse_complement = "G" + reverse_complement
        elif ch == "G":
            reverse_complement = "C" + reverse_complement
    return reverse_complement

if __name__ == "__main__":
    filename = argv[1]
    sequences = fastaparser(filename)
    for key in sequences:
        revcomp = complement(sequences[key])
        forward = translateorfs(sequences[key])
        reverse = translateorfs(revcomp)
        total = forward + reverse
        total = set(total)
        for i in total:
            print i
        

