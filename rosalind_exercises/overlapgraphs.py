#!/usr/bin/evn python
"""
A script that returns an overlap graph, with an overlap of a defined postive
integer.

Given:
-- A collection of DNA strings in FASTA format having total length at most 10kbp.

Return:
-- The adjacency list corresponding to overlap x. Edges are returned in any order.
"""

from sys import argv

__author__ = "Lotte Witjes"
__date__ = "14th November 2016"
__version__ = "1"
__email__ = "lotte.witjes@wur.nl"

def fastaParser(filename):
    """Parses sequences in FASTA format.

    Keyword arguments:
    filename -- the name of the input file containing the sequences in FASTA format.

    Return arguments:
    seqs -- a dictionary with the headers as keys and the sequences as values.
    """
    thefile = open(filename, "r")
    seqs = {}
    for line in thefile:
        if line.startswith(">"):
            label = line.strip().replace(">", "")
            seqs[label] = ""
        else:
            seqs[label] += line.strip()
    return seqs

def overlapGraphs(dictionary, overlap):
    """Finds duos of overlapping sequences with overlap x.

    Keyword arguments:
    dictionary -- a dictionary containing sequences with headers as keys.
    overlap -- a positive integer indicating the overlap in basepairs.

    Return arguments:
    listOverlap -- a list of lists containing the duos of overlapping sequences.
    """
    listOverlap = []
    for key1 in dictionary:
        for key2 in dictionary:
            if dictionary[key1][-overlap:] == dictionary[key2][:overlap] and key1 != key2:
                duo = [key1, key2]
                listOverlap.append(duo)
    return listOverlap

def writeOutputOverlapGraphs(listOverlap):
    for i in listOverlap:
        sequence1, sequence2 = i
        print sequence1 + " " + sequence2
    

if __name__ == "__main__":
    filename = argv[1]
    seqs = fastaParser(filename)
    listOverlap = overlapGraphs(seqs, 3)
    writeOutputOverlapGraphs(listOverlap)
    
