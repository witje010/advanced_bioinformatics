from __future__ import division

#!/usr/bin/evn python
"""Returns the nucleotide sequence with the highest GC content."""

from sys import argv

def GCcontent(nucleotides):
    nucleotides = nucleotides.upper()
    total = len(nucleotides)
    c = nucleotides.count("C")
    g = nucleotides.count("G")
    GCcontent = ((c+g)/total)*100.0
    return GCcontent

if __name__ == "__main__":
    filename = argv[1]
    thefile = open(filename, "r")
    maxheader = None
    maxGC = 0

    sequence = ""
    alist = []
    lines = thefile.readlines()
    last = lines[-1]
    stripped_lines = []
    
    for i in range(len(lines)):
        line = lines[i].strip("\n")
        stripped_lines.append(line)
        
    
    for line in stripped_lines:
        if line.startswith(">") and sequence != "":
            GC = GCcontent(sequence)
            alist.append([header, GC])
            sequence = ""
            line = line.strip(">")
            line = line.strip("\n")
            header = line
        elif line.startswith(">") and sequence == "":
            line = line.strip(">")
            line = line.strip("\n")
            header = line
        elif line == last:
            sequence += line
            GC = GCcontent(sequence)
            alist.append([header, GC])
        else:
            sequence += line

    for i in range(len(alist)):
        if alist[i][1] >= maxGC:
            maxGC = alist[i][1]
            maxheader = alist[i][0]
        else:
            pass
    
    print maxheader
    print "{:.6f}".format(maxGC)
   
    
    
    
