#!/usr/bin/evn python
"""
Author = Lotte Witjes
Student number = 950405 966 120

Returns start indices of N-glycosylation motifs for each protein given in a
.txt file of Uniprot access IDs.
"""

from sys import argv
import requests
import re

def getIDs(filename):
    thefile = open(filename, "r")

    accessIDs = []
    for line in thefile:
        accessIDs.append(line.strip())
    return accessIDs

def getFastaParseFasta():
    seqs = {}
    IDs = getIDs(filename)
    for i in range(len(IDs)):
        url = "http://www.uniprot.org/uniprot/{}.fasta".format(IDs[i])
        fasta = requests.get(url, stream=True)
        fasta = fasta.text
        elements = fasta.split("\n")
        elements = elements[1:]
        stripped_elements = []
        for elem in elements:
            j = str(elem)
            j = j.strip("u")
            if j == "":
                pass
            else:
                stripped_elements.append(j)
        sequence = "".join(stripped_elements)
        seqs[IDs[i]] = sequence
    return seqs
            
def findMotif():
    seqs = getFastaParseFasta()
    pattern = re.compile(r"(?=[N][ACDEFGHIKLMNQRSTVWY][ST][ACDEFGHIKLMNQRSTVWY])")
    for key in seqs:
        matches = re.finditer(pattern, seqs[key])
        show = re.findall(pattern,seqs[key])
        if show != []:
            print key
            for i in matches:
                print i.start() + 1,
            print ""
    

if __name__ == "__main__":
    filename = argv[1]
    findMotif()
    
    
