#!/usr/bin/evn python
"""Complement a given DNA sequence."""

from sys import argv

def complement(string):
    string = string.upper()
    reverse_complement = ""
    for ch in string:
        if ch == "A":
            reverse_complement = "T" + reverse_complement
        elif ch == "T":
            reverse_complement = "A" + reverse_complement
        elif ch == "C":
            reverse_complement = "G" + reverse_complement
        elif ch == "G":
            reverse_complement = "C" + reverse_complement
    return reverse_complement

if __name__ == "__main__":
    filename = argv[1]
    thefile = open(filename, "r")
    for line in thefile:
        result = complement(line)
    print result
