#!/usr/bin/evn python
"""
Author: Lotte Witjes
Student number: 950405 966 120

A program that reports the first index of a specified motif in
a specified DNA string.
"""

from sys import argv
import re

def findMotifs(filename):
    thefile = open(filename, "r")
    lines = thefile.readlines()

    stripped_lines = []
    for line in lines:
        stripped_lines.append(line.strip())
                    
    DNAseq = stripped_lines[0]
    motif = stripped_lines[1]
    motifREGEX = re.compile(r"(?="+motif+")")
    matches = re.finditer(motifREGEX, DNAseq)
    indexlist = []
    for i in matches:
        print i.start() + 1,

if __name__ == "__main__":
    filename = argv[1]
    findMotifs(filename)


