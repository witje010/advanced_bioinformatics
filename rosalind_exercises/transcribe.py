#!/usr/bin/evn python
"""Transcribes DNA into RNA."""

from sys import argv

def transcribe(string):
    """Transcribes DNA into RNA."""
    string = string.upper()
    rna = string.replace("T", "U")
    return rna

if __name__ == "__main__":
    filename = argv[1]
    thefile = open(filename, "r")
    for line in thefile:
        result = transcribe(line)
    print result
