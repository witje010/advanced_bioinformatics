from __future__ import division

#!/usr/bin/evn python
"""
Author: Lotte Witjes
Student number: 950405 966 120

Input: a fastQ file
Output: a trimmed fastQ file and a file containing average quality scores per base before and after
trimming.

python exercise_P3.py inputfilename outputtrimmed outputtable
"""

from sys import argv
import subprocess

def parseFastq(filename):
	thefile = open(filename, "r")
	seqs = {}
	for line in thefile:
		if line.startswith("@"):
			label = line.strip().replace("@", "")
			seqs[label] = []
		elif line.startswith(("+", "-")):
			strand = line.strip()
			seqs[label].append(strand)
		else:
			seqs[label].append(line.strip())
	return seqs

def translatePhredScores(filename):
	seqs = parseFastq(filename)
	for key in seqs:
		line = ""
		for ch in seqs[key][2]:
			score = str(ord(ch)-64)
			line += score + " "
		seqs[key][2] = line.strip()
	return seqs

def calculateLengths(filename):
	seqs = translatePhredScores(filename)
	for key in seqs:
		length = len(seqs[key][0])
		seqs[key].append(length)
	return seqs

def calculateMinMaxAverageLengths(filename):
	seqs = calculateLengths(filename)
	values = seqs.values()
	lengths = []
	for i in range(len(values)):
		length = values[i][3]
		lengths.append(length)
	minLength = min(lengths)
	maxLength = max(lengths)
	total = 0
	for i in lengths:
		total += int(i)
	averageLength = total / len(lengths)
	return minLength, maxLength, averageLength

def averageQualityScorePerBase(filename):
	seqs = calculateLengths(filename)
	values = seqs.values()
	qualityScores = []
	for i in range(len(values)):
		qualityScore = values[i][2].split()
		qualityScores.append(qualityScore)
	
	dictionary = {}
	for i in range(len(qualityScores[0])):
		dictionary[i+1] = []
	
	for i in range(len(qualityScores)):
		for j in range(len(qualityScores[i])):
			dictionary[j+1].append(qualityScores[i][j])
	
	for key in dictionary:
		total = 0
		length = len(dictionary[key])
		for i in dictionary[key]:
			total += int(i)
		dictionary[key] = total / length

	return dictionary

def runFastqQualityTrimmer(filename):
	cmd = "fastq_quality_trimmer -t 30 -Q 64 -i {0} -o {1}".format(filename, "trimmed.fq")
	res = subprocess.check_call(cmd, shell=True)
	return res
			
if  __name__ == "__main__":
	inputFile = argv[1]
	outputtable = "results_table.fq"
	
	output = runFastqQualityTrimmer(inputFile)
	
	statsBefore = calculateMinMaxAverageLengths(inputFile)
	qualityBefore = averageQualityScorePerBase(inputFile)

	statsAfter = calculateMinMaxAverageLengths("trimmed.fq")
	qualityAfter = averageQualityScorePerBase("trimmed.fq")
	print "ORIGINAL: min=" + str(statsBefore[0]) + ", max=" + str(statsBefore[1]) + ", avg=" + str(statsBefore[2])
	print "TRIMMED: min=" + str(statsAfter[0]) + ", max=" + str(statsAfter[0]) + ", avg=" + str(statsAfter[2])
	
	for key in qualityBefore:
		difference = float(qualityAfter[key]) - float(qualityBefore[key])
		line = "{}	{:.2f}	{:.2f}	{:.2f}".format(key, qualityBefore[key], qualityAfter[key], difference)
		print line
