from __future__ import division

#!/usr/bin/evn python
"""
Author: Lotte Witjes
Student number: 950405 966 120

A script that parses the output of Needle alignment between a reference sequence and (multiple) related sequences and prints a summary table on the screen.
The script contains a function that calculates the Hamming distance between two sequences of EQUAL length, it doesn't work for sequences of unequal length (will give error).
The script contains a function that calculates the percentage identity between two sequences. If the sequences are of unequal length, please define the Hamming distance as parameter.

Run the script from the command line as follows:
$ python exercise_P5_lottewitjes.py (your reference fasta) (your related fasta)

The script produces one output file, which is the output file of the Needle alignment, containing all alignments that had been performed. The file is called 'out.needle'.
"""

from sys import argv
import subprocess
import os.path

def fastaparser(filename):
	"""Parses a .fasta file into a dictionary where keys are accessions codes and values are the actual sequences."""
	thefile = open(filename, "r")
	seqs = {}
	for line in thefile:
		if line.startswith(">"):
			label = line.strip().replace(">", "")
			elements = label.split()
			seqs[elements[0]] = []
		else:
			seqs[elements[0]] += line.strip()
	
	for key in seqs:
		seqs[key] = ["".join(seqs[key])]
	
	for key in seqs:
		if "|" in key:
			elements = key.split("|")
			seqs[elements[2]] = seqs[key]
			del seqs[key]
	return seqs

def determineSequenceLengths(filename):
	"""Returns the dictionary as in the function fastaparser but now the length of the sequence is added in the values of the keys."""
	seqs = fastaparser(filename)
	for key in seqs:
		length = len(seqs[key][0])
		seqs[key].append(length)
	return seqs

def runNeedle(inputReference, inputRelated, gapopen, gapextend):
	"""Runs Needle alignment with sequence1, versus sequence2, with defined gapopen and gapextend values. The output is always out.needle.
	The program checks if this output file already exists, if it does than Needle isn't executed."""
	output = "out.needle"
	cmd = "needle -asequence {0} -bsequence {1} -gapopen {2} -gapextend {3} -outfile {4}".format(inputReference, inputRelated, gapopen, gapextend, output)
	if os.path.exists(output):
		pass
	else:
		e = subprocess.check_output(cmd, shell=True)
		res = subprocess.check_call(cmd, shell=True)
		return res

def hammingDistance(sequence1, sequence2):
	"""Returns the number of different amino acids/bases between two sequences of equal length"""
	if len(sequence1) == len(sequence2):
		difference = 0
		for i in range(len(sequence1)):
			if sequence1[i] != sequence2[i]:
				difference += 1
	return differences

def percentageIdentity(sequence1, sequence2, alignmentLength, hamming=None):
	"""Returns the percentage identity between two sequences of equal length or different length. If sequences of different length, define Hamming distance. 
	The percentage identity is defined as identical amino acids or bases divided by the alignment length times 100."""
	if hamming == None:
		hamming = hammingDistance(sequence1, sequence2)
		numberOfIdenticalPositions = len(sequence1) - float(hamming)
		percentOfIdentity = (float(numberOfIdenticalPositions) / float(alignmentLength))*100
	else:
		numberOfIdenticalPositions = len(sequence1) - float(hamming)
		percentOfIdentity = (float(numberOfIdenticalPositions) / float(alignmentLength))*100
	return percentOfIdentity

def parseNeedleIDs(output):
	"""Parses the IDs of the Needle output into a list in the same order as the output."""
	thefile = open(output, "r")
	infoNeedleIDs = []
	for line in thefile:
		if line.startswith("# 2:"):
			elements = line.split()
			infoNeedleIDs.append(elements[2])
	return infoNeedleIDs


def parseNeedleHamming(output):
	"""Parses the Hamming distance of the Needle output into a list in the same order as the output."""
	thefile = open(output, "r")
	infoNeedleSimilarity = []
	infoNeedleLength = []
	for line in thefile:
		if line.startswith("# Similarity:"):
			elements = line.split()
			elements = elements[2].split("/")
			infoNeedleSimilarity.append(elements[0])
			infoNeedleLength.append(elements[1])
	infoNeedleHamming = []
	for i in range(len(infoNeedleLength)):
		hamming = int(infoNeedleLength[i]) - int(infoNeedleSimilarity[i])
		infoNeedleHamming.append(hamming)
	return infoNeedleHamming
	
def parseNeedleIdentity(output):
	"""Parses the percentage identities of the Needle output into a list in the same order as the output."""
	thefile = open(output, "r")
	infoNeedleIdentity = []
	for line in thefile:
		if line.startswith("# Identity:"):
			elements = line.split()
			identity = elements[3]
			identity = identity[1:-2]
			infoNeedleIdentity.append(identity)
	return infoNeedleIdentity

def writeOutput(referenceSeqs, relatedSeqs, IDs, hamming, identity):
	"""Writes a summary of the Needle output on the screen. Showing the ID and length of sequence1 and sequence2, the Hamming distance, and the percentage identity."""
	print "Sequence1	Length	Sequence2	Length	Hamm	Ident"
	for i in range(len(IDs)):
		line = "{0}	{1}	{2}	{3}	{4}	{5}".format("GPA1_ARATH", referenceSeqs["GPA1_ARATH"][1], IDs[i], relatedSeqs[IDs[i]][1], hamming[i], identity[i])
		print line
		

if  __name__ == "__main__":
	inputReference = argv[1]
	inputRelated = argv[2]
	output = "out.needle"
	referenceSeqs = determineSequenceLengths(inputReference)
	relatedSeqs = determineSequenceLengths(inputRelated)
			
	runNeedle(inputReference, inputRelated, 8, 0.5)

	IDs = parseNeedleIDs(output)
	hamming = parseNeedleHamming(output)
	identity = parseNeedleIdentity(output)
	writeOutput(referenceSeqs, relatedSeqs, IDs, hamming, identity)
