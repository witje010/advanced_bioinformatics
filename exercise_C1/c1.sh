#!/bin/bash
#Lotte Witjes	C1	950405 966 120

sizeseq -descending Y -sequences plants.fasta -outseq stdout | nthseq -sequence stdin -number 1 -outseq stdout | pepstats -sequence stdin -outfile stdout | grep "Residues = " | cut -d " " -f 7
