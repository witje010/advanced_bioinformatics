from __future__ import division

#!/usr/bin/evn python
"""
Author: Lotte Witjes
Student number: 950405 966 120

Obtains GB files from accession numbers provided in a .txt file. Parses these GB
files into two output files. One file containing the shortest sequence in fasta
format. The other file containing a tab-delimited table of accession number,
organism name and sequence length.
"""

from sys import argv
from Bio import Entrez
import re
from exercise_P2 import *

def retrieveGBfiles(inputIDs, inputGB):
    thefile = open(inputIDs, "r")
    for line in thefile:
        IDs = line.split()
    Entrez.email = "lotte.witjes@wur.nl"
    handle = Entrez.efetch(db="nucleotide", id=IDs, rettype="gb")
    output = open(inputGB, "w")
    for line in handle.read():
        output.write(line)
    output.close()

def mergeInfo(inputGB):
    accessions = extractAccessions(inputGB)
    organisms = extractOrganisms(inputGB)
    sequences = extractSequences(inputGB)
    GCcontents = extractGCs(inputGB)
    lengths = extractSequenceLength(inputGB)
    info = []
    for i in range(len(accessions)):
        infolist = [lengths[i], GCcontents[i], organisms[i], accessions[i], sequences[i][0]]
        info.append(infolist)       
    info.sort()
    return info

def writeOutputTable(inputGB):
    mergedInfo = mergeInfo(inputGB)
    thefile = open("output_table.txt", "w")
    for i in range(len(mergedInfo)):
        organism = "{:35s}".format(mergedInfo[i][2])
        thefile.write(str(mergedInfo[i][3]) + "\t" + str(organism) + "\t" + str(mergedInfo[i][0]) + "\n")
    thefile.close()

def writeOutputFasta(inputGB):
    mergedInfo = mergeInfo(inputGB)
    thefile = open("output_smallest_sequence.txt", "w")
    thefile.write(">" + str(mergedInfo[0][3]) + " " + str(mergedInfo[0][2]) + "\n")
    thefile.write(str(mergedInfo[0][4]) + "\n")
    thefile.close()
    
if __name__ == "__main__":
    inputIDs = "input.txt"
    inputGB = "outputGB.txt"
    retrieveGBfiles(inputIDs, inputGB)
    writeOutputTable(inputGB)
    writeOutputFasta(inputGB)
    
