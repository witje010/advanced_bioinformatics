from __future__ import division

#!/usr/bin/evn python
"""
Author: Lotte Witjes
Student number: 950405 966 120

Reads a GenBank file and parses information into two separate output files.

The first file contains: FASTA file with the sequences, ordered from high to
low GC content. Labels should be accession numbered followed by organism name.

The second file contains: A tab-delimited file with the following columns:
1.Accession number
2.Organism name
3.GC content (printed as percentage with two decimals)
4.Sequence length
The lines should be ordered from high to low GC content.
"""

from sys import argv
import re

def extractAccessions():
    accessions = []
    thefile = open(inputGB, "r")
    for line in thefile:
        if line.startswith("ACCESSION"):
            elements = line.split()
            accession = elements[1]
            accessions.append(accession)
    return accessions

def extractOrganisms():
    organisms = []
    thefile = open(inputGB, "r")
    for line in thefile:
        if line.startswith("  ORGANISM"):
            elements = line.split()
            elements = elements[1:]
            organism = " ".join(elements)
            organisms.append(organism)
    return organisms

def extractSequences():
    sequencelist = []
    sequencepattern = re.compile(r"^(\s)+([0123456789]+)(\s)(\s)([atcg]+)")
    thefile = open(inputGB, "r")
    for line in thefile:
        match = re.search(sequencepattern, line)
        if match != None:
            sequencelist.append(line.strip())
    sequences = []
    sequence = ""
    for i in range(len(sequencelist)):
        if sequencelist[i].startswith("1  ") and sequence != "":
            sequences.append([sequence])
            sequence = ""
        else:
            sequence += "".join([j for j in sequencelist[i] if not j.isdigit()])
    sequences.append([sequence])

    strippedsequences = []
    for i in range(len(sequences)):
        stripped = sequences[i][0].strip()
        joined = stripped.replace(" ", "")
        strippedsequences.append([joined])
    return strippedsequences

def extractGCs():
    sequences = extractSequences()
    GCcontents = []
    for i in range(len(sequences)):
        GC = GCcontent(sequences[i][0])
        GCcontents.append(GC)
    return GCcontents

def GCcontent(nucleotides):
    nucleotides = nucleotides.upper()
    total = len(nucleotides)
    c = nucleotides.count("C")
    g = nucleotides.count("G")
    GCcontent = ((c+g)/total)*100.0
    return GCcontent

def extractSequenceLength():
    sequences = extractSequences()
    lengths = []
    for i in range(len(sequences)):
        length = len(sequences[i][0])
        lengths.append(length)
    return lengths

def mergeInfo():
    accessions = extractAccessions()
    organisms = extractOrganisms()
    sequences = extractSequences()
    GCcontents = extractGCs()
    lengths = extractSequenceLength()
    info = []
    for i in range(len(accessions)):
        infolist = [GCcontents[i], accessions[i], organisms[i], lengths[i], sequences[i][0]]
        info.append(infolist)       
    info.sort(reverse=True)
    return info

def writeOutputFasta(outputFasta):
    info = mergeInfo()
    thefile = open(outputFasta, "w")
    for i in range(len(info)):
        thefile.write(">" + str(info[i][1]) + " " + str(info[i][2]) + "\n")
        thefile.write(str(info[i][4]) + "\n")
    thefile.close()

def writeOutputTable(outputTable):
    info = mergeInfo()
    thefile = open(outputTable, "w")
    for i in range(len(info)):
        organism = "{:35s}".format(info[i][2])
        thefile.write(str(info[i][1]) + "\t" + str(organism) + "\t")
        GCcontent = "{:.2f}".format(info[i][0])
        thefile.write(str(GCcontent) + "%" + "\t" + str(info[i][3]) + "\n")
    thefile.close()

if __name__ == "__main__":
    inputGB = argv[1]
    outputFASTA = argv[2]
    outputTABLE = argv[3]
    try:
        writeOutputFasta(outputFASTA)
        writeOutputTable(outputTABLE)
        print "Files made!"
    except:
        print "Something went wrong!"

                
    
    
